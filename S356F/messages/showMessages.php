<?php
session_start();
// error_reporting(0);

include_once '../db_conn.php';

$to_username = $_REQUEST["to_username"];
$from_username = $_REQUEST["from_username"];

// get all messages related to this two user
$sql = "SELECT from_user_email, to_user_email, content, time, opened FROM message WHERE (from_user_email = '". $from_username ."' AND to_user_email = '".$to_username ."' ) OR (from_user_email = '". $to_username ."' AND to_user_email = '".$from_username ."' ) ";
$result = mysqli_query($conn, $sql);

// get messages that is send by currently used user that is read by others
$sql = "SELECT from_user_email, to_user_email, content, time, opened FROM message WHERE from_user_email = '". $from_username ."'  AND opened = 1";
$myMsgOpenedResult = mysqli_query($conn, $sql);

// get the last message read by others
$sql = "SELECT from_user_email, to_user_email, content, time, opened FROM message WHERE from_user_email = '". $from_username ."'  AND opened = 1 ORDER BY time DESC LIMIT 1";
$myLastMsgOpenedResult = mysqli_query($conn, $sql);

$outputText = "";
$totalMyMsg = mysqli_num_rows($myMsgOpenedResult);
$totalMsg = mysqli_num_rows($result);

while ($row = mysqli_fetch_array($result)) {
  if ($totalMsg == 1) {
    if ($row['from_user_email'] == $from_username) {
      if ($row['opened'] == 1 && $totalMyMsg == 1) {
        $outputText = $outputText."
        <div class='message-dialog message-by-me col-xs-12' id='js-last-messages-box'>
          <i class='fas fa-check-double double-tick'></i><div class='inner-message-box'>".$row['content']."</div>
        </div>";
      } else {
        $outputText = $outputText."
        <div class='message-dialog message-by-me col-xs-12' id='js-last-messages-box'>
          <div class='inner-message-box'>".$row['content']."</div>
        </div>";

      }
      $totalMyMsg = $totalMyMsg - 1;
    }
    if ($row['to_user_email'] == $from_username) {
      $outputText = $outputText."
      <div class='message-dialog message-by-others col-xs-12' id='js-last-messages-box'>
        <div class='inner-message-box inner-other-message'>".$row['content']."</div>
      </div>";
    }
    break;
  }

  if ($row['from_user_email'] == $from_username) {
    if ($row['opened'] == 1 && $totalMyMsg == 1) {
      $outputText = $outputText."
      <div class='message-dialog message-by-me col-xs-12'>
        <i class='fas fa-check-double double-tick'></i><div class='inner-message-box'>".$row['content']."</div>
      </div>";
    } else {
      $outputText = $outputText."
      <div class='message-dialog message-by-me col-xs-12'>
        <div class='inner-message-box'>".$row['content']."</div>
      </div>";

    }
    $totalMyMsg = $totalMyMsg - 1;
  }
  if ($row['to_user_email'] == $from_username) {
    $outputText = $outputText."
    <div class='message-dialog message-by-others col-xs-12'>
      <div class='inner-message-box inner-other-message'>".$row['content']."</div>
    </div>";
  }
  $totalMsg = $totalMsg - 1;
}
echo $outputText;

// set the received message to opened
$sql = "UPDATE message SET opened = 1 WHERE from_user_email = '". $to_username ."' AND to_user_email = '".$from_username ."'  ";
mysqli_query($conn, $sql);

mysqli_free_result ($result);
mysqli_free_result ($myMsgOpenedResult);
mysqli_free_result ($myLastMsgOpenedResult);

?>
