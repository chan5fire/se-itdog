<!DOCTYPE html>
<!--
        ustora by freshdesignweb.com
        Twitter: https://twitter.com/freshdesignweb
        URL: https://www.freshdesignweb.com/ustora/
-->
<?php
session_start();

include_once 'db_conn.php';

include_once 'header.php';

$_SESSION['username'];

$_SESSION['NickName'];

echo "
  <script>
    var fromUsername = '".$_SESSION['username']."';
    var toUsername = undefined;
  </script>
";

if (!empty($_GET['to_username'])) {
  $to_username = $_GET['to_username'];
  echo "
  <script>
  var toUsername = '".$to_username."';
  </script>
  ";
}
if (!empty($_GET['search_username'])) {
  $search_username = $_GET['search_username'];
}

?>

<?php
/*
  <!--- start mainmenu-area --->
  <div class="mainmenu-area">
  <div class="container">
  <div class="row">
  <div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
  <span class="sr-only">Toggle navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  </button>
  </div>
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  <ul class="nav navbar-nav">
  <li class="active"><a href="./">Home</a></li>
  <li><a href="#">Shop</a></li>
  <li><a href="#">About Us</a></li>
  </ul>

  <div class="search-container">
  <form class="navbar-form navbar-left" action="?" method="POST">

  <input id="search" name="item" type="text" class="form-control" placeholder="Search" size="50">

  <button type="submit" name="search_it" class="btn btn-default">Submit</button>

  </form>
  <?php
  if (isset($_POST['search_it'])) {

  $_SESSION['search_condition'] = $_POST['item'];

  if ($_SESSION['search_condition'] != NULL) {
  echo "<script>window.location = 'search_result.php'</script>";
  //header("Location: search_result.html");
  }
  }
  ?>
  </div>

  <ul class="nav navbar-nav navbar-right">
  <?php
  if ($_SESSION['username'] == NULL) {
  echo "<li><a href='login_page.php'>Login</a></li>";
  } else {
  echo "<li class='dropdown'>
  <a href='personal_info.php' class='dropdown-toggle' data-toggle='dropdown'>" .'Hi, '. $_SESSION['NickName'] . "<span class='caret'></span></a>
  <ul class='dropdown-menu' role='menu'>
  <li><a href='personal_info.php'>Info</a></li>
  <li><a href='#'>My Shop</a></li>
  <li class='divider'></li>
  <li><a href='#'>Buy Record</a></li>
  <li><a href='#'>Sell Record</a></li>
  <li class='divider'></li>
  <li><a href='logout.php'>Logout</a></li>
  </ul>
  </li>";
  }
  ?>
  </ul>

  </div>
  </div>
  </div>
  </div> <!-- End mainmenu area -->
 */
?>
<div class="container">
  <div class="message-page row">
    <div class="left-side-message col-xs-12 col-sm-4">
      <div class="row">
        <h1 class="col-xs-12 message-title">Message</h1>
        <form class="col-xs-12 search-name-form" method="get">
          <input type="text" name="search_username" placeholder="email of user you want to chat" class="search-name-input"/>
          <input type="submit" value="search"/>
        </form>
        <?php
          $i = 0;
          if (!empty($_GET['search_username'])) {
            $search_name_query = "SELECT Name, Email_Address FROM personal_info WHERE Email_Address NOT LIKE '".$_SESSION['username'] ."' AND Email_Address LIKE '%".$search_username ."%' ";
            $search_name_result = mysqli_query($conn, $search_name_query);
            if ($no_of_search_name_rows = mysqli_num_rows($search_name_result) == 0) {
              echo "<div class='col-xs-12'>Sorry, no user matches!</div>";
            } else {
              while ($name_result_row = mysqli_fetch_array($search_name_result, MYSQLI_ASSOC)) {
                $to_username = $name_result_row["Email_Address"];
                $last_msg = "";
                $message_all_query = "SELECT content from message WHERE from_user_email LIKE '".$to_username ."' OR to_user_email LIKE '".$to_username ."' ";
                $message_all_row = mysqli_query($conn, $message_all_query);

                while ($msg_row = mysqli_fetch_array($message_all_row, MYSQLI_ASSOC)) {
                  $last_msg = $msg_row['content'];
                }

                echo "
                <a class='message-selection row' href='messages.php?to_username=". $name_result_row["Email_Address"]."'>
                <div class='message-username col-xs-12'>"
                .$name_result_row['Name'] ." (". $name_result_row['Email_Address']." )
                </div>
                <div class='col-xs-12' id='message-last'>"
                .$last_msg."
                </div>
                </a>";
              }
            }
          } else {
            $username_all_query = "SELECT Name, Email_Address FROM personal_info WHERE Email_Address NOT LIKE '".$_SESSION['username'] ."' ";
            $username_all_result = mysqli_query($conn, $username_all_query);

            while ($username_all_row = mysqli_fetch_array($username_all_result, MYSQLI_ASSOC)) {
              $to_username = $username_all_row["Email_Address"];
              $last_msg = "";
              $message_all_query = "SELECT content from message WHERE from_user_email LIKE '".$to_username ."' OR to_user_email LIKE '".$to_username ."' ";
              $message_all_row = mysqli_query($conn, $message_all_query);

              while ($msg_row = mysqli_fetch_array($message_all_row, MYSQLI_ASSOC)) {
                $last_msg = $msg_row['content'];
              }

              echo "
              <a class='message-selection row' href='messages.php?to_username=". $username_all_row["Email_Address"]."'>
              <div class='message-username col-xs-12'>"
              .$username_all_row['Name'] ." (". $username_all_row['Email_Address']." )
              </div>
              <div class='col-xs-12' id='message-last'>".$last_msg."
              </div>
              </a>";
            }
          }
        ?>


      </div>
    </div>

    <div class="right-side-message col-xs-12 col-sm-7">
      <?php
      if (empty($_GET['to_username'])) {
        echo "
          <div class='message-default-hint-box'>
            Click username in left hand side to start chatting
          </div>
        ";
      } else {
        $query = "SELECT Name from personal_info WHERE Email_Address LIKE '".$_GET['to_username'] ."' ";
        $result = mysqli_query($conn, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $to_user_nikename = $row['Name'];

        echo "
          <div class='message-history-box row'>
            <div id='to-username' class='message-username right-message-username col text-center'>".$to_user_nikename."(".$_GET['to_username'].")</div>
            <div id='js-messages-box'></div>
            <div class='message-input-box col-xs-12'>
              <form class='row' style='display: flex;justify-content: space-between;'>
                <input type='text' class='message-textarea col-xs-9' id='entered-content' placeholder='Text your message here'/>
                <input type='button' class='col-xs-2' value='send' onclick='sendMessage()' >
              </form>
            </div>
            <div id='txtHint'></div>
          </div>
        ";
      }
      ?>

    </div>
  </div>

</div>


<?php
include_once 'footer.php';
/*
  <div class="footer-bottom-area">
  <div class="container">
  <div class="row">
  <div class="col-md-8">
  <div class="copyright">
  <p>&copy; 2015 uCommerce. All Rights Reserved. <a href="http://www.freshdesignweb.com" target="_blank">freshDesignweb.com</a></p>
  </div>
  </div>

  <div class="col-md-4">
  <div class="footer-card-icon">
  <i class="fa fa-cc-discover"></i>
  <i class="fa fa-cc-mastercard"></i>
  <i class="fa fa-cc-paypal"></i>
  <i class="fa fa-cc-visa"></i>
  </div>
  </div>
  </div>
  </div>
  </div> <!-- End footer bottom area -->

  <!-- Latest jQuery form server -->
  <script src="https://code.jquery.com/jquery.min.js"></script>

  <!-- Bootstrap JS form CDN -->
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

  <!-- jQuery sticky menu -->
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.sticky.js"></script>

  <!-- jQuery easing -->
  <script src="js/jquery.easing.1.3.min.js"></script>

  <!-- Main Script -->
  <script src="js/main.js"></script>

  <!-- Slider -->
  <script type="text/javascript" src="js/bxslider.min.js"></script>
  <script type="text/javascript" src="js/script.slider.js"></script>
  </body>
  </html>
 */
?>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
  setTimeout( function() { scrollToBottom(); }, 3000);
  setInterval( function() { showChat(); }, 1000);
  setInterval( function() { showLastMessage(); }, 1000);
});


function showChat() {
  if (toUsername !== undefined) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("js-messages-box").innerHTML = this.responseText;
      }
    };
    xmlhttp.open("GET", "messages/showMessages.php?from_username=" + fromUsername + "&to_username=" + toUsername , true);
    xmlhttp.send();
  }
}

function showLastMessage() {
  if (toUsername !== undefined) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("message-last").innerHTML = this.responseText;
      }
    };
    xmlhttp.open("GET", "messages/lastMessage.php?to_username=" + toUsername , true);
    xmlhttp.send();
  }
}

function sendMessage() {
  var msgContent = document.getElementById("entered-content").value.length !== 0 ? document.getElementById("entered-content").value : "";
  document.getElementById("entered-content").value = "";
  if (toUsername !== undefined && msgContent.length !== 0) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST","messages/sendMessage.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        setTimeout( function() { scrollToBottom(); }, 1000);
      }
    }
    xmlhttp.send("from_username=" + fromUsername + "&to_username=" + toUsername + "&msg_content=" + msgContent);
  }
}

function scrollToBottom() {
  var element = document.getElementById("js-last-messages-box");
  if (element == null) {
    return;
  }
  element.scrollIntoView(true);
}
</script>
