<?php
    session_start();
    error_reporting(0);
    include_once 'header.php';
?>
<link href="http://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet" type="text/css">

<style>
    h3 {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
    }
    p.light {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
    }
    </style>

<!-- Page Content -->
<div class="container" style="margin-bottom: 40px;">

    <!-- Introduction Row -->
    <div class="col-md-12">
        <h2 class="page-header">About Us</h2>
        <p>Our mission is to make phone trading at fingertips, easily accessible and informative for both buyers and sellers by implementing the latest technology.</p> 
        <p>Fill in the contact form below for support. Or you can contact us at <a href="mailto: support@it9.com">support@it9.com</a></p>
        <p>Stay tuned by following us on <a href="https://facebook.com/it9">Facebook</a> or <a href="https://twitter.com/it9">Twitter</a>.</p>
    </div>

    <!-- Contact Form -->
    <?php
        include_once 'contactUs.php';
    ?>
    
    <!-- Team Members Row -->
    <div class="col-md-12">
        <h2 class="page-header">Our Team
            <small>Meet people behind IT9</small>
        </h2>
    </div>
   Those information are deleted.

</div>

<?php
    include_once 'footer.php';
?>