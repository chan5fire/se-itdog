<?php
if(isset($_POST['del_commemt_but'])){
	if(isset($_POST['del_commemt_target'])){
		require 'db_conn.php';
		
		$target = $_POST['del_commemt_target'];
		
		$sql = "DELETE FROM comments WHERE com_id=?";
		$stmt = mysqli_stmt_init($conn);
		
		if(mysqli_stmt_prepare($stmt, $sql)){
			mysqli_stmt_bind_param($stmt, "i", $target);
			if(mysqli_stmt_execute($stmt)){
				echo    "<script>
			                alert('You comment has been deleted!');
					        location.href = '".$_SERVER['HTTP_REFERER']."&delete=success';
					    </script>";
				exit();
			} else {
				echo    "<script>
			                alert('Error occur!');
					        location.href = '".$_SERVER['HTTP_REFERER']."';
					    </script>";//&error=execute
			}
		} else {
			echo    "<script>
			            alert('Error occur!');
					    location.href = '".$_SERVER['HTTP_REFERER']."';
					</script>";//&error=prepare
		}
		
		
	} else {
		echo    "<script>
				    location.href = '".$_SERVER['HTTP_REFERER']."';
				</script>";
		exit();
	}
} else {
	header("Location: index.php");
	exit();
}