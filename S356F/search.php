<?php

// Attempt MySQL server connection.
$conn = mysqli_connect("localhost", "root", "", "s356f");
 
// Check connection
if($conn === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
 
if(isset($_REQUEST["term"])){
	
	// Prepare a select statement
	$sql = "select * from (SELECT CONCAT(Brand, ' ', Product_Name) AS Result, Item_ID FROM product_info) as tb1 WHERE RESULT LIKE ? ORDER BY Result LIMIT 5";
        
    if($stmt = mysqli_prepare($conn, $sql)){
        
		
		// Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "s", $param_term);
        
        // Set parameters
		$param_term = '%' . TRIM($_REQUEST["term"]) . '%' ;
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            $result = mysqli_stmt_get_result($stmt);
            
            // Check number of rows in the result set
            if(mysqli_num_rows($result) > 0){
                // Fetch result rows as an associative array
                while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){           
					echo '<a class="autocomplete_item" href="buy_item.php?item_id='.$row['Item_ID'].'">'.$row['Result'].'</a>';
                }
            } else{
                echo "<p>No match found</p>";
            }
        } else{
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
        }
		// Close statement
		mysqli_stmt_close($stmt);
    }else {
    echo mysqli_error($conn);
}
}

// close connection
mysqli_close($conn);