<?php
    session_start();
    error_reporting(0);
	include_once 'db_conn.php';
    include_once 'header.php';
	if(!$_SESSION['isAdmin']){
			echo '<script type="text/javascript">window.location="index.php"</script>';
	}
?>
<link href="http://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet" type="text/css">

<style>
    h3 {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
    }
    p.light {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
    }
    .img-thumbnail:hover {
    position:relative;
    width:250px;
    height:auto;
    display:block;
    z-index:999;
}
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{
    vertical-align: middle;
    }
</style>

<script>
	function deleteCheck(){
		return confirm("Are you sure to delete this user?");
	}
</script>

<div class="container" style="margin-bottom: 40px;">
	<table>
		<form action="remove_item.php" id="removeItem" style="margin-bottom: 0px;"></form>
		<th><button type="submit" form="removeItem" class="btn btn-default">Delete Item</button></th>
		<form action="remove_user.php" id="removeUser" style="margin-bottom: 0px;"></form>
		<th><button type="submit" form="removeUser" class="btn btn-default" disabled>Delete User</button></th>
	</table>
	<form role="form" method="post">
<?php
	$query1 = "SELECT * FROM `personal_info` WHERE isAdmin ='0' ";
	$result = mysqli_query($conn, $query1);
		echo "	<table class='table'>
							<tr>
								<th>Nickname</th>
								<th>Real Name</th>
								<th>Email Address</th>
								<th>Gender</th>
								<th>Join Date</th>
								<th>Delete User</th>
							</tr>";
							
		while($row = mysqli_fetch_assoc($result)){
					echo "	<tr>
								<td>". $row['Name']. "</td>
								<td>". $row['FirstName']. " ". $row['LastName']. "</td>
								<td><a href='others_info.php?email=".$row['Email_Address']."'>". $row['Email_Address']. "</td>
								<td>". $row['Gender']. "</td>
								<td>". $row['Create_Date']. "</td>
								<td><button type='submit' name='delete' value=". $row['Email_Address']. " onClick='return deleteCheck()'>Delete</button></td>
							</tr>";
				}
		echo "</table>"
?>
	</form>
</div>
<?php
    if (isset($_POST['delete'])) {
		include_once 'db_conn.php';

            $temp = "DELETE FROM personal_info WHERE Email_Address = '" . $_POST['delete'] . "'";
			$temp2 = "DELETE FROM login WHERE Login_Email = '" . $_POST['delete'] . "'";
			$temp3 = "DELETE FROM product_info WHERE Seller_Email = '" . $_POST['delete'] . "'";
			$temp4 = "DELETE FROM comments WHERE post_email = '" . $_POST['delete'] . "'";
			$temp5 = "DELETE FROM message WHERE from_user_email = '" . $_POST['delete'] . "'";
			$temp6 = "DELETE FROM message WHERE to_user_email = '" . $_POST['delete'] . "'";
			$temp7 = "DELETE FROM shopping_cart WHERE Email_Address = '" . $_POST['delete'] . "'";
			$sql = "Select url as Target_url from product_info Where Seller_Email='". $_POST['delete'] ."'";
			
			$result = mysqli_query($conn, $sql);
			while($row = mysqli_fetch_assoc($result)){
				$Target_url = $row['Target_url'];
				unlink($Target_url);
			}
			//echo $temp;

            if (mysqli_query($conn, $temp) && mysqli_query($conn, $temp2) && mysqli_query($conn, $temp3) && mysqli_query($conn, $temp4) && mysqli_query($conn, $temp5) && mysqli_query($conn, $temp6) && mysqli_query($conn, $temp7)) {
                echo '<script>window.alert("User Deleted!")</script>';
                echo "<script>window.location = 'remove_user.php'</script>";
            } else {
            echo '<script>window.alert("Server connection failed!")</script>';
			}
       }  
?>
<?php
    include_once 'footer.php';
?>