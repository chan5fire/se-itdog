<?php
session_start();
$_SESSION['message'] = "";
  // Message Vars
  $msg = '';
  $msgClass = '';

  // Check For Submit
  if(filter_has_var(INPUT_POST, 'submit')){
    // Get Form Data
    $name = htmlspecialchars($_POST['name']);
    $email = htmlspecialchars($_POST['email']);
    $subject = htmlspecialchars($_POST['subject']);
    $message = htmlspecialchars($_POST['message']);

    // Check Required Fields
    if(!empty($email) && !empty($name) && !empty($message)){
        // Fill in all field
        // Check Email
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
          // Passed
          echo '<script type="text/javascript">';
          echo 'alert("Message sent! Thank you for contacting us!");';
          echo 'window.location.href = "about_us.php";';
          echo '</script>';

        }else
        // Failed
          $msg = 'Invalid email';
          $msgClass = 'alert-danger';

      }else {
      // not yet fill in all field
      $msg = 'Please fill in all the required fields';
      $msgClass = 'alert-danger';
      }
    }
?>

<!-- Contact Us -->
<div class="col-md-12">
    <h2 class="page-header">Contact Us</h2>
	<form id="contact-form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">

        <div class="form-group">
        <label>User ID
        <span class="required">*</span></label>
			<input name="name" type="text" class="form-control" placeholder="Your userID" value="<?php echo isset($_POST['name']) ? $name : ''; ?>">
		</div>

        <div class="form-group">
        <label for="email">E-mail
        <span class="required">*</span></label>
			<input name="email" type="text" id="email" class="form-control" placeholder="Your Email" value="<?php echo isset($_POST['email']) ? $email : ''; ?>">
        </div>

        <div class="form-group">
        <label for="subject">Subject</label>
			<input name="subject" type="text" id="subject" class="form-control" placeholder="Email Subject"  value="<?php echo isset($_POST['subject']) ? $subject : ''; ?>">
        </div>

        <div class="form-group">
        <label for="message">Message
        <span class="required">*</span></label>
			<textarea name="message" type="text" id="message" class="form-control" placeholder="Message or Question" ><?php echo isset($_POST['message']) ? $message : ''; ?></textarea>
        </div>

        <p><span class="required">*</span> Required fields</p>

        <div class="form-group">
			<button name="submit" type="submit" id="contact-submit">Send Message</button>
		</div>

	</form>

    <?php if($msg != ''): ?><div class="alert <?php echo $msgClass; ?>"><?php echo $msg; ?></div><?php endif; ?>

</div>
