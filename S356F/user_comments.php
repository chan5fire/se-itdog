<?php
session_start();

error_reporting(0);

include_once 'header.php';

include_once 'db_conn.php';
?>

<div class="container" style="margin-bottom: 40px;">
	<div class="col-md-12">

<?php
	echo "<h2 class='page-header'>Comments</h2>";
	$sql = "SELECT * FROM comments WHERE seller = ? ORDER BY post_datetime DESC, com_id";
	$stmt = mysqli_stmt_init($conn);
	
	if(mysqli_stmt_prepare($stmt, $sql)){
		mysqli_stmt_bind_param($stmt, "s", $_SESSION['username']);
		mysqli_stmt_execute($stmt);
		$result = mysqli_stmt_get_result($stmt);
		
		if(!mysqli_num_rows($result) > 0){
			echo "You do not get a comment!";
		} else {
			while($row = mysqli_fetch_assoc($result)){
				echo	"<div class='show_comment_box'>";               //show the comment div			
				echo	"   <p class='comment_info'>By <em>".$row['post_nickname']."</em> ".$row['post_datetime']."</p>"
							."<p>".nl2br($row['content'])."</p>".//comment
						"</div>";                                      //close the comment div
			}
		}
		
	}

?>
	</div>
</div>


<?php
    include_once 'footer.php';
?>