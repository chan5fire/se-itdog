<?php
    session_start();
    error_reporting(0);
	include_once 'db_conn.php';
    include_once 'header.php';
	$query = "SELECT * FROM personal_info WHERE Email_Address = '" . $_SESSION['username'] . "'";
	$result = mysqli_query($conn, $query);
	$row1 = mysqli_fetch_array($result);
	$nickname = $row1["Name"];
	$firstname = $row1["FirstName"];
	$lastname = $row1["LastName"];
	$gender = $row1["Gender"];
?>

<div class="container" style="margin-bottom: 40px;">
    <div class="col-md-12">
        <h2 class="page-header">Change Personal Information</h2>
        <form role="form" method="post">
            <div class="form-group">
                <label for="nickname">Nickname</label>
                <input type="text" class="form-control" name="Name" placeholder="Enter new nickname" required value= "<?php echo htmlspecialchars($nickname); ?>"/>
            </div>
			
			<table  style="width:100%">
			<tr><td style="padding: 0px 7px 0px 0px; width: 50%;">
            <div class="form-group">
                <label for="lastName">Last Name</label>
                <input type="text" class="form-control" name="LastName" placeholder="Enter new last name" required value= "<?php echo htmlspecialchars($firstname); ?>"/>
            </div>
			</td>
			<td style="padding: 0px 0px 0px 7px; width: 50%;">
			<div class="form-group">
                <label for="firstName">First Name</label>
                <input type="text" class="form-control" name="FirstName" placeholder="Enter new first name" required value= "<?php echo htmlspecialchars($lastname); ?>" />
            </div>
			</td></tr></table>
			
			<div class="form-group">
                <label for="gender">Gender</label>
					<select name="gender" class="form-control">
						<option <?php if($gender == 'male'){echo("selected");}?> value="male">Male</option>
						<option <?php if($gender == 'female'){echo("selected");}?> value="female">Female</option>
						<option <?php if($gender == 'other'){echo("selected");}?> value="other">Others</option>
					</select>
			</div>
            <div class="btn-group">
                <button type="submit" name="submit" value="Submit" class="btn btn-default">Submit</button>
                <button type="submit" class="btn btn-default" onclick="location.href='personal_info.php';return false;">Cancel</button>
            </div>
        </form>
    </div>
</div>

<?php

if ($_SESSION['username'] == NULL) {
    header('Location: login_page.php');
}

if (isset($_POST['submit'])) {
		$n_nickname = $_POST['Name'];
		$n_firstname = $_POST['LastName'];
		$n_lastname = $_POST['FirstName'];
		$n_gender = $_POST['gender'];
		
            include_once 'db_conn.php';

            $temp = "UPDATE personal_info SET Name = '" . $n_nickname . "'" . ", FirstName = '" . $n_firstname . "'" . " , LastName = '" . $n_lastname . "'  , Gender = '" . $n_gender . "'" . " WHERE Email_Address = '" . $_SESSION['username'] . "'";
			//echo $temp;

            if (mysqli_query($conn, $temp)) {
                echo '<script>window.alert("Personal information changed!")</script>';
                echo "<script>window.location = 'personal_info.php'</script>";
            } else {
            echo '<script>window.alert("Server connection failed!")</script>';
       }		
} 
?>


<?php
    include_once 'footer.php';
?>

