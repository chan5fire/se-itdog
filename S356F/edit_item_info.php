<?php
session_start();
error_reporting(0);
include_once 'header.php';

$id = $_GET['item_id'];
?>

<div class="container" style="margin-bottom: 40px;">
    <div class="col-md-12">
        <h2 class="page-header">Edit Item Info</h2>
        <?php
			include_once 'db_conn.php';
			
			$sql = "SELECT * FROM product_info WHERE Item_ID = ?";
			$stmt = mysqli_stmt_init($conn);
			
			mysqli_stmt_prepare($stmt, $sql);
			mysqli_stmt_bind_param($stmt, "i", $id);
			mysqli_stmt_execute($stmt);
			
			$result = mysqli_stmt_get_result($stmt);
			$row = mysqli_fetch_assoc($result);
			
			//check identity of the user
			if(empty($_SESSION['username'])){ 													//check login
				echo '<script>alert("Please login before editing your product!")</script>';
				echo '<script>location = "login_page.php"</script>';
				exit();
			} else if($row['Seller_Email'] != $_SESSION['username']){							//whether user is the owner of the product
				echo '<script>alert("You do not own this product!")</script>';
				echo '<script>location = "index.php"</script>';
				exit();
			}
			
			//Display image of that product
			echo 	"<div style='text-align:center;'>
						<img src='".$row['url']."' class='img-responsive' style='height: 35vh; width: 80%; display:inline-flex; object-fit: contain;'>
					</div>";
		?>
        <form role="form" method="post">
			<!--new code-->
			<div class="form-group">
                <label for="Brand">Brand</label>
                <input class="form-control" type="text" name="Brand" id="Brand" placeholder="Enter New Brand" <?php echo "value = '".$row['Brand']."'";?> required>
            </div>
			<div class="form-group">
                <label for="Product_Name">Phone model</label>
                <input class="form-control" type="text" name="Product_Name" id="Product_Name" placeholder="Enter New Phone Model" <?php echo "value = '".$row['Product_Name']."'";?> required>
            </div>
			
            <div class="form-group">
                <label for="Price">Price</label>
                <input class="form-control" type="number" min="1" name="Price" id="Price" placeholder="Enter New Price" <?php echo "value = '".$row['Price']."'";?> required>
            </div>
			<div class="form-group">
                <label for="New_Or_Old">Phone status</label>
                <select name = "isnew"class="form-control" id="New_Or_Old" required>
                    <option value="" selected disabled hidden>Select Phone status</option>
                    <option value="T" <?php echo "selected";?>>Brand new</option>
                    <option value="F" <?php if($row['New'] == "F") echo "selected";?>>Old</option>
                </select>
            </div>
            <div class="form-group">
                <label for="Depreciation_Rate">Depreciation Rate</label>
                <input class="form-control" type="number" min="0" max="100" name="Depreciation_Rate" id="Depreciation_Rate" placeholder="Enter New depreciation rate"
					<?php 	if($row['New'] == "T"){
								echo "value = '100'";
							} else {
								echo "value = '".$row['Depreciation_Rate']."'";
							}?>
				required>
            </div>
            <div class="form-group">
                <label for="Description">Description</label>
                <input class="form-control" type="text" name="Description" id="Description" placeholder="Enter New Description" <?php echo "value = '".$row['Description']."'";?> required>
            </div>
            <div class="btn-group">
                <button type="submit" name="submit" value="Submit" class="btn btn-default">Submit</button>
                <button type="submit" class="btn btn-default" onclick="location.href='javascript:history.back()';return false;">Cancel</button>
            </div>
        </form>
    </div>
</div>

<?php

$p = $_POST['Price'];
$dr = $_POST['Depreciation_Rate'];
$d = $_POST['Description'];
//new code
$brand = $_POST['Brand'];
$Product_Name = $_POST['Product_Name'];
$isNew = $_POST['isnew'];


if (isset($_POST['submit'])) {
    if(!empty($p) && !empty($dr) && !empty($d) && !empty($brand) && !empty($Product_Name) && !empty($isNew)){
        include_once 'db_conn.php';
		//new code
		if(!empty($brand)){
            $sql = "UPDATE product_info SET Brand = '" . $brand . "'" . " WHERE Item_ID = '".$id."'";
            mysqli_query($conn, $sql);
        }
		if(!empty($Product_Name)){
            $sql = "UPDATE product_info SET Product_Name = '" . $Product_Name . "'" . " WHERE Item_ID = '".$id."'";
            mysqli_query($conn, $sql);
        }
		if(!empty($isNew)){
            $sql = "UPDATE product_info SET New = '" . $isNew . "'" . " WHERE Item_ID = '".$id."'";
            mysqli_query($conn, $sql);
        }
		
        if($p != ""){
            $query10 = "UPDATE product_info SET Price = '" . $p . "'" . " WHERE Item_ID = '".$id."'";
            mysqli_query($conn, $query10);
        }
        if($dr != ""){
            $query11 = "UPDATE product_info SET Depreciation_Rate = '" . $dr . "'" . " WHERE Item_ID = '".$id."'";
            mysqli_query($conn, $query11);
        }
        if($d != ""){
            $query12 = "UPDATE product_info SET Description = '" . $d . "'" . " WHERE Item_ID = '".$id."'";
            mysqli_query($conn, $query12);
        }
        echo '<script>window.alert("Item Information Changed!")</script>';
        echo '<script>window.location = "buy_item.php?item_id='.$id.'"</script>';
    } else {
        echo'<script>window.alert("Please enter information for change!")</script>';
    }
}
?>


<?php
include_once 'footer.php';
?>