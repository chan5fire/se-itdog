<?php
    session_start();
    error_reporting(0);
    include_once 'db_conn.php';
    $emailaddress = $_SESSION['username'];
    include_once 'header.php';
?>
<link href="http://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet" type="text/css">

<style>
    h3 {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
    }
    p.light {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
    }
    .img-thumbnail:hover {
    position:relative;
    width:250px;
    height:auto;
    display:block;
    z-index:999;
}
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{
    vertical-align: middle;
    }
</style>

<!-- js -->
<script>
//add all price and go check out
//delete the product



</script>

<!-- Page Content -->
<div class="container" style="margin-bottom: 40px;">

<form action="checkoutNdel.php" method="GET">

<table class="table">
<thead>
    <tr>
        <th>Product Photo</th>
        <th>Product Name</th>
        <th>Price</th>
        <th>Brand</th>
        <th>Upload Date</th>
        <th>Seller</th>
        <th>Depreciation Rate</th>
    </tr>
</thead>
<tbody>

<?php 
$count = 0;
$Item_ID = array();
$sql= "Select s.Email_Address, s.Item_ID, s.cartID, p.* from shopping_cart s, product_info p where s.Item_ID = p.Item_ID and s.Email_Address = '$emailaddress' and p.Sell_Date = '0000-00-00' ";

if($result = mysqli_query($conn, $sql)){
    while($row = mysqli_fetch_array($result)){
        //$Item_ID =$row["Item_ID"];
        //echo $Item_ID;

        echo "<tr>";
        echo "<div class='form-check'>";
        echo "<td class = 'product_img'>" . "<input class='form-check-input position-static' type='checkbox' id=".$count." name='check_list[]' value=".$row["cartID"].">" . "<img src = ". $row["url"]." class='img-thumbnail' height='25%' width='25%'>" . "</a>" . "</td>";
        echo "</div>";
        echo "<td class = 'product_name'>" . $row["Product_Name"]. "</td>";
        echo "<td class = 'product_price'>" . "$" . $row["Price"]. "</td>";
        echo "<td class = 'product_brand'>" . $row["Brand"]. "</td>";
        echo "<td class = 'product_uDate'>" . $row["Upload_Date"]. "</td>";
        echo "<td class = 'product_sellerEmail'>" . $row["Seller_Email"]. "</td>";
        echo "<td class = 'productDRate'>" . $row["Depreciation_Rate"]. "</td>";
        echo "</tr>";
        $count++;

    }
}

?>
</tbody>

</table>

<input type="submit" name="delFromCart" value="Delete"/>
<input type="submit" name="checkOut" value="Check Out"/>

</form>

</div><!-- for container -->
<?php
    include_once 'footer.php';
?>