<?php
    session_start();
    error_reporting(0);
	include_once 'db_conn.php';
    include_once 'header.php';
	if(!$_SESSION['isAdmin']){
			echo '<script type="text/javascript">window.location="index.php"</script>';
	}
?>
<link href="http://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet" type="text/css">

<style>
    h3 {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
    }
    p.light {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
    }
    .img-thumbnail:hover {
    position:relative;
    width:250px;
    height:auto;
    display:block;
    z-index:999;
}
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{
    vertical-align: middle;
    }
</style>

<script>
	function deleteCheck(){
		return confirm("Are you sure to delete this item?");
	}
</script>

<div class="container" style="margin-bottom: 40px;">
	<table>
		<form action="remove_item.php" id="removeItem" style="margin-bottom: 0px;"></form>
		<th><button type="submit" form="removeItem" class="btn btn-default" disabled>Delete Item</button></th>
		<form action="remove_user.php" id="removeUser" style="margin-bottom: 0px;"></form>
		<th><button type="submit" form="removeUser" class="btn btn-default">Delete User</button></th>
	</table>
	<form role="form" method="post">
<?php
	$query1 = "SELECT Item_ID, Product_Name, Price, Brand, Seller_Email ,url FROM `product_info`";
	$result = mysqli_query($conn, $query1);
		echo "	<table class='table'>
							<tr>
								<th>Item ID</th>
								<th>Product Name</th>
								<th>Price</th>
								<th>Brand</th>
								<th>Seller</th>
								<th>Photo</th>
								<th>Delect Product</th>
							</tr>";
							
		while($row = mysqli_fetch_assoc($result)){
					echo "	<tr>
								<td>". $row['Item_ID']. "</td>
								<td><a href='buy_item.php?item_id=".$row['Item_ID']."'>". $row['Product_Name']. "</td>
								<td>". $row['Price']. "</td>
								<td>". $row['Brand']. "</td>
								<td><a href='others_info.php?email=".$row['Seller_Email']."'>". $row['Seller_Email']. "</td>
								<td><img src='/s356f/".$row['url']."' style='height: 200px; width: 80%; display:inline-flex; object-fit: contain;''/></td>
								<td><button type='submit' name='delete' value=". $row['Item_ID']. " onClick='return deleteCheck()'>Delete</button></td>
							</tr>";
				}
		echo "</table>"
?>
	</form>
</div>
<?php
    if (isset($_POST['delete'])) {

            $temp = "DELETE FROM product_info WHERE Item_ID = '" . $_POST['delete'] . "'";
			$temp2 = "DELETE FROM shopping_cart WHERE Item_ID = '" . $_POST['delete'] . "'";
			$sql = "Select url as Target_url from product_info Where Item_ID='". $_POST['delete'] ."'";
			
			$result = mysqli_query($conn, $sql);
			$fetched_result = mysqli_fetch_assoc($result);
			$Target_url = $fetched_result['Target_url'];
			
			//echo $temp;

            if (mysqli_query($conn, $temp) && unlink($Target_url) && mysqli_query($conn, $temp2)) {
                echo '<script>window.alert("Item deleted!")</script>';
                echo "<script>window.location = 'remove_item.php'</script>";
            } else {
            echo '<script>window.alert("Server connection failed!")</script>';
			}
       }  
?>
<?php
    include_once 'footer.php';
?>

