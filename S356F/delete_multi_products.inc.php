<?php
if(isset($_POST['delete_multi_button'])){
	if(isset($_POST['delete_targets'])){
		$delete_targets = $_POST['delete_targets'];
		//echo '<script>alert('.$delete_targets.');</script>';
	} else {
		//echo '<script>alert("Test");</script>';
		echo 	'<script>
					alert("No product is selected");
					location.href = "my_shop.php";
				</script>';
	}
	
	require 'db_conn.php';
	
	$delete_sql = "Delete from product_info where Item_ID=?";
	$stmt = mysqli_stmt_init($conn);
	
	//if(mysqli_stmt_prepare($stmt, $delete_sql)){
		foreach($delete_targets as $value){
			//delete image
			$sql = "Select url as Target_url from product_info Where Item_ID=?"; //find image url
			if(mysqli_stmt_prepare($stmt, $sql)){
				mysqli_stmt_bind_param($stmt, "i", $value);
				mysqli_stmt_execute($stmt);
				$result = mysqli_stmt_get_result($stmt);
				$fetched_result = mysqli_fetch_assoc($result);
				$Target_url = $fetched_result['Target_url'];
				//echo 	$Target_url;
			
				if(!unlink($Target_url)){	//delete image
					header("Location: my_shop.php?error=deleteImgError");
					exit();
				}
			} else {
				header("Location: my_shop.php?error=deleteImgPrepareError");
				exit();
			}
			
			//delete record
			if(mysqli_stmt_prepare($stmt, $delete_sql)){
				mysqli_stmt_bind_param($stmt, "i", $value);
				if(mysqli_stmt_execute($stmt)){
					//echo 	'<script>
					//			alert("Product is deleted!");
					//		</script>';
				} else {
					header("Location: my_shop.php?error=deleteExecuteError");
					exit();
				}
			} else {
				header("Location: my_shop.php?error=deletePrepareError");
				exit();
			}
			
		}
		echo 	'<script>
					alert("Products are deleted!");
					location.href = "my_shop.php";
				</script>';
	//}else{
	//	header("Location: my_shop.php?error=prepareError");
	//	exit();
	//}
	
	
} else {
	//echo '<script>alert("Test2");</script>';
	header("Location: my_shop.php");
	exit();
}