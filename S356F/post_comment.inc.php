<?php
if(isset($_POST['comment_button'])){
	require 'db_conn.php';
	
	$seller = $_POST['seller'];
	$post_email = $_POST['post_email'];
	$post_nickname = $_POST['post_nickname'];
	$message = $_POST['message'];
	
	//handle error
	//check emptyfield
	if(empty($seller) || empty($post_email) || empty($post_nickname)){
		header("Location: ". $_SERVER['HTTP_REFERER']);
        exit;
	} else {
		//check emptyfield
		if(empty($message)){
			echo    "<script>
			            alert('Please input your comment!');
						location.href = '".$_SERVER['HTTP_REFERER']."&error=emptyfield';
					</script>";
		} else {
			$sql = "INSERT INTO comments (seller, post_email, post_nickname, content) VALUES (?, ?, ?, ?)";
			$stmt = mysqli_stmt_init($conn);
			
			if(mysqli_stmt_prepare($stmt, $sql)){
				mysqli_stmt_bind_param($stmt, "ssss", $seller, $post_email, $post_nickname, $message);
				if(mysqli_stmt_execute($stmt)){
					header("Location: ".$_SERVER['HTTP_REFERER']."&comment=success");
					exit();
				} else {
					echo    "<script>
			                    alert('Error occur!');
						        location.href = '".$_SERVER['HTTP_REFERER']."';
					        </script>";
				}
			} else {
				echo    "<script>
			                alert('Error occur!');
						    location.href = '".$_SERVER['HTTP_REFERER']."';
					    </script>";
			}
		}
	}
	mysqli_stmt_close($stmt);
    mysqli_close($conn);
	
} else {
	header("Location: index.php");
	exit();
}